import tensorflow as tf
from tqdm import tqdm
import os

class Trainer(object):
    # a trainer using the feature & label scheme
    # features and labels must both be numpy.ndarray
    # features.shape[0] == labels.shape[0] must hold
    def __init__(self, model_graph, loss_graph, features, labels, save_to, load_from=None,
                 train_op=None, preprocess_fn=None, batch_size=32, buffer_size=10000, learning_rate=0.0001):
        """
        
        :param model_graph: x -> y
        :param loss:  y, t -> l
        :param train_op:  a function takes a loss tensor, and returns a train op
        :param features: 
        :param labels: 
        :param preprocess_fn: 
        :param batch_size: 
        :param buffer_size: 
        """
        assert features.shape[0] == labels.shape[0]
        self.features_placeholder = tf.placeholder(dtype=features.dtype, shape=features.shape)
        self.labels_placeholder = tf.placeholder(dtype=labels.dtype, shape=labels.shape)
        if preprocess_fn is not None:
            self.dataset = tf.data.Dataset.from_tensor_slices((self.features_placeholder, self.labels_placeholder)).map(preprocess_fn).shuffle(buffer_size).batch(batch_size)
        else:
            self.dataset = tf.data.Dataset.from_tensor_slices((self.features_placeholder, self.labels_placeholder)).shuffle(buffer_size).batch(batch_size)
        self.iterator = self.dataset.make_initializable_iterator()
        self.x, self.t = self.iterator.get_next()
        
        self.y = model_graph(self.x)
        self.loss = loss_graph(self.y, self.t)
        
        if train_op is None:
            self.train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss)
        else:
            self.train_op = train_op(self.loss)
        
        
        self.init_op = tf.global_variables_initializer()
        gpu_ops = tf.GPUOptions(allow_growth=True)
        config = tf.ConfigProto(gpu_options=gpu_ops, log_device_placement=False)
        self.sess = tf.Session(config=config)
        self.sess.run(self.init_op)
        self.saver = tf.train.Saver()
        
        if load_from is not None:
            self.saver.restore(self.sess, load_from)
    
        self.save_to = save_to
        return
            
    def load(self, path):
        self.saver.restore(self.sess, path)
        
    def save(self, path):
        self.saver.save(self.sess, path)
        
    def run(self, n_epoch):
        for i in range(n_epoch):
            loss = 0.0
            cnt = 0
            while True:
                try:
                    _, tmp = self.sess.run([self.train_op, self.loss])
                    loss += tmp
                    cnt += 1
                except tf.errors.OutOfRangeError:
                    break
            print('epoch', i, 'loss', loss / cnt)
            self.save(self.save_to)
        self.save(self.save_to)
        return
    

class Validator(object):
    def __init__(self, sess, model_graph, loss_graph, features, labels, preprocess_fn=None, batch_size=32):
        self.sess = sess
        assert features.shape[0] == labels.shape[0]
        self.features_placeholder = tf.placeholder(dtype=features.dtype, shape=features.shape)
        self.labels_placeholder = tf.placeholder(dtype=labels.dtype, shape=labels.shape)
        if preprocess_fn is not None:
            self.dataset = tf.data.Dataset.from_tensor_slices((self.features_placeholder, self.labels_placeholder)).map(
                preprocess_fn).batch(batch_size)
        else:
            self.dataset = tf.data.Dataset.from_tensor_slices(
                (self.features_placeholder, self.labels_placeholder)).batch(batch_size)
        self.iterator = self.dataset.make_initializable_iterator()
        self.x, self.t = self.iterator.get_next()

        self.y = model_graph(self.x, reuse=True)
        self.loss = loss_graph(self.y, self.t)

        return
        
        
    def run(self):
        loss = 0.0
        cnt = 0
        while True:
            try:
                tmp = self.sess.run(self.loss)
                loss += tmp
                cnt += 1
            except tf.errors.OutOfRangeError:
                break
        print('validation loss', loss / cnt)
        return
    
                    
class InterleaveTrainer(object):
    # a trainer using the feature & label scheme
    # features and labels must both be numpy.ndarray
    # features.shape[0] == labels.shape[0] must hold
    def __init__(self, model_graph, loss_graph, x_train, x_valid, t_train, t_valid, save_to, load_from=None,
                 train_op=None, preprocess_fn=None, batch_size=32, buffer_size=4096, learning_rate=0.00001,
                 log_path=None):
        """

        :param model_graph: x -> y
        :param loss:  y, t -> l   # if it is an lambda, make sure it takes two args
        :param train_op:  a function takes a loss tensor, and returns a train op
        :param features: 
        :param labels: 
        :param preprocess_fn: # if it is an lambda, make sure it takes two args
        :param batch_size: 
        :param buffer_size: 
        """
        assert x_train.shape[0] == t_train.shape[0]
        assert x_valid.shape[0] == t_valid.shape[0]
        
        self.x_train = x_train
        self.x_valid = x_valid
        self.t_train = t_train
        self.t_valid = t_valid
        
        self.x_train_ph = tf.placeholder(dtype=x_train.dtype, shape=x_train.shape)
        self.x_valid_ph = tf.placeholder(dtype=x_valid.dtype, shape=x_valid.shape)
        self.t_train_ph = tf.placeholder(dtype=t_train.dtype, shape=t_train.shape)
        self.t_valid_ph = tf.placeholder(dtype=t_valid.dtype, shape=t_valid.shape)
        
        self.batch_size = batch_size
        
        if preprocess_fn is not None:
            self.dataset_train = tf.data.Dataset.from_tensor_slices(
                (self.x_train_ph, self.t_train_ph)).map(preprocess_fn).shuffle(buffer_size).batch(batch_size)
            self.dataset_valid = tf.data.Dataset.from_tensor_slices(
                (self.x_valid_ph, self.t_valid_ph)).map(preprocess_fn).batch(batch_size)
        else:
            self.dataset_train = tf.data.Dataset.from_tensor_slices(
                (self.x_train_ph, self.t_train_ph)).shuffle(buffer_size).batch(batch_size)
            self.dataset_valid = tf.data.Dataset.from_tensor_slices(
                (self.x_valid_ph, self.t_valid_ph)).batch(batch_size)
        self.iterator_train = self.dataset_train.make_initializable_iterator()
        self.iterator_valid = self.dataset_valid.make_initializable_iterator()
        self.x_batch_train, self.t_batch_train = self.iterator_train.get_next()
        self.x_batch_valid, self.t_batch_valid = self.iterator_valid.get_next()

        self.y_batch_train = model_graph(self.x_batch_train)
        self.y_batch_valid = model_graph(self.x_batch_valid, reuse=True)
        
        self.loss_train = loss_graph(self.y_batch_train, self.t_batch_train)
        self.loss_valid = loss_graph(self.y_batch_valid, self.t_batch_valid)

        if train_op is None:
            self.train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(self.loss_train)
        else:
            self.train_op = train_op(self.loss_train)



        self.init_op = tf.global_variables_initializer()
        gpu_ops = tf.GPUOptions(allow_growth=True)
        config = tf.ConfigProto(gpu_options=gpu_ops, log_device_placement=False)
        self.sess = tf.Session(config=config)
        self.sess = tf.Session()
        if log_path is None:
            self.summary_op = tf.no_op()
        else:
            self.log_path = log_path
            self.summary_op = tf.summary.merge_all()
            path = os.path.join(log_path, 'train')
            self.train_writer = tf.summary.FileWriter(path, self.sess.graph)
            path = os.path.join(log_path, 'valid')
            self.valid_writer = tf.summary.FileWriter(path, self.sess.graph)
        self.sess.run(self.init_op)
        self.saver = tf.train.Saver()

        if load_from is not None:
            self.saver.restore(self.sess, load_from)

        self.save_to = save_to
        return

    def load(self, path):
        self.saver.restore(self.sess, path)

    def save(self, path):
        try:
            self.saver.save(self.sess, path)
        except KeyboardInterrupt:
            print('Keyboard interrupted, saving to', path, ' before terminating')
            self.saver.save(self.sess, path)
            exit(0)
        return

    def run(self, n_epoch):
        for i in range(n_epoch):
            self.sess.run(self.iterator_train.initializer, {self.x_train_ph: self.x_train, self.t_train_ph: self.t_train})
            self.sess.run(self.iterator_valid.initializer, {self.x_valid_ph: self.x_valid, self.t_valid_ph: self.t_valid})
            loss = 0.0
            cnt = 0
            pbar = tqdm(total=self.x_train.shape[0] / self.batch_size, desc='Training epoch ' + str(i))
            while True:
                pbar.update()
                try:
                    summary, _, tmp = self.sess.run([self.summary_op, self.train_op, self.loss_train])
                    loss += tmp
                    cnt += 1
                except tf.errors.OutOfRangeError:
                    break
            if self.log_path is not None:
                self.train_writer.add_summary(summary, i)
            pbar.close()
            print('epoch', i, 'training loss', loss / cnt)
            self.save(self.save_to)
                
            loss = 0.0
            cnt = 0
            pbar = tqdm(total=self.x_valid.shape[0] / self.batch_size, desc='Validation epoch ' + str(i))
            while True:
                pbar.update()
                try:
                    summary, tmp = self.sess.run([self.summary_op, self.loss_valid])
                    loss += tmp
                    cnt += 1
                except tf.errors.OutOfRangeError:
                    break
            if self.log_path is not None:
                self.valid_writer.add_summary(summary, i)
            pbar.close()
            print('epoch', i, 'validation loss', loss / cnt)
        self.save(self.save_to)
        return