from utils import pipeline
import numpy as np


        

class DataFeeder_MappingPath(object):
    def __init__(self, x_path, t_path, x_pattern, filename_map_kernel, loader_fn, batch_size=1, shuffle=True):
        self.all_x = pipeline.search_files(dir=x_path, pattern=x_pattern)
        self.capacity = len(self.all_x)
        assert self.capacity >= batch_size
        self.all_t = [pipeline.filename_map(x_path, t_path, x, filename_map_kernel) for x in self.all_x]
        self.batch_size = batch_size
        self.all_path = np.array([self.all_x, self.all_t]).T
        if shuffle:
            self.shuffle()
        self.iterator = 0
        self.loader_fn = loader_fn
        return

    def shuffle(self):
        np.random.shuffle(self.all_path)
        self.iterator = 0
        return

    def __iter__(self):
        return self

    def __next__(self):
        if self.iterator + self.batch_size > self.capacity:
            self.shuffle()
        xs = []
        ts = []
        for i in range(self.iterator, self.iterator+self.batch_size):
            x_path, t_path = self.all_path[i]
            x, t = self.loader_fn(x_path, t_path)
            xs.append(x)
            ts.append(t)
        self.iterator += self.batch_size
        xs = np.array(xs)
        ts = np.array(ts)
        return xs, ts
    
