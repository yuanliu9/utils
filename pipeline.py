import os
import re

def filename_map(src, dst, x, kernel):
    """
    map a data filename (fullpath) to corresponding label filename (fullpath),
    which resides in a mirror directory dst having the same structure as src
    """
    dirpath, basename = os.path.split(x)
    basename_t = kernel(basename)
    relpath = os.path.relpath(dirpath, src)
    fullpath_t = os.path.join(os.path.join(dst, relpath), basename_t)
    return fullpath_t


def parse_dir_kernel(dirpath):
    """
    parse a nested folder structure into a list of files in fullpath
    :param dir: 
    :return: 
    """
    subdirs = os.listdir(dirpath)
    parsed_dirs = []
    for subdir in subdirs:
        fullpath = os.path.join(dirpath, subdir)
        if os.path.isdir(fullpath):
            parsed_dirs.append(parse_dir_kernel(fullpath))
        else:
            parsed_dirs.append(fullpath)
    return parsed_dirs


def parse_dir(dir: str) -> [str]:
    """
    parse a nested dir, find all files in the target dir
    # NOTE: use with caution when there exists symbolic links in the dir,
    #       a looped symbolic may cause infinite parsing as this case is not considered and tested in implementation
    :param dir: a str of a directory, may contain one ~ as a shortcut for $HOME
    :return: a list of str contains all the full paths of files inside the directory
    """
    parsed_dirs = parse_dir_kernel(dir)
    flatten = lambda l: sum(map(flatten, l), []) if isinstance(l, list) else [l]
    return flatten(parsed_dirs)


def filter_files(file_list: [str], pattern: str) -> [str]:
    """
    filter the files in a given pattern, files that do not match this pattern will be filtered out
    :param file_list: 
    :return: 
    """
    res = []
    for fullpath in file_list:
        basename = os.path.basename(fullpath)
        dirname = os.path.dirname(fullpath)
        pattern = re.compile(pattern=pattern)
        match = pattern.fullmatch(basename)
        if match is not None:
            res.append(fullpath)
        else:
            #print(fullpath)
            pass
    return res


def search_files(dir: str, pattern: str) -> [str]:
    return filter_files(parse_dir(dir), pattern)


def replace_path(src, dst, fullpath):
    relpath = os.path.relpath(fullpath, start=src)
    newpath = os.path.join(dst, relpath)
    return newpath


def replace_ext(path, ext):
    base, ext_old = os.path.splitext(path)
    return base+ext


def replace_path_and_create_dir(src, dst, fullpath):
    newpath = replace_path(src, dst, fullpath)
    new_dirname = os.path.dirname(newpath)
    os.makedirs(new_dirname, exist_ok=True)
    return newpath


def pipe(src_dir, dst_dir, f, pattern):
    """
    mapping all operations in all the files and sub-files in src_dir with pattern on function f,
    and saving the result to dst_dir
    :param src_dir: 
    :param dst_dir: 
    :param f: must take two string, the src and dst full filepaths
    :param pattern: necessary to filter out unwanted system managed files
    :return: 
    """
    src_dir = os.path.expanduser(src_dir)
    dst_dir = os.path.expanduser(dst_dir)
    parsed_dir = parse_dir(src_dir)
    filtered_dir = filter_files(parsed_dir, pattern)
    [mmp for mmp in map(lambda fullpath: f(fullpath, replace_path_and_create_dir(src_dir, dst_dir, fullpath)), filtered_dir)]
    return 
    

def pipe_ext(src_dir, dst_dir, f, pattern, ext):
    pipe(src_dir, dst_dir, lambda src, dst: f(src, replace_ext(dst, ext)), pattern)
