import tensorflow as tf

class BaseModel(object):
    def __init__(self, sess=None):
        if sess is None:
            sess = tf.Session()
        self.sess = sess
        self.loss = None
        self.train_op = None
        self.x = None
        self.y = None
        self.t = None
        return

    def predict(self, x):
        return self.sess.run(self.y, {self.x: x})

    def fit(self, x, t):
        _, loss = self.sess.run([self.train_op, self.loss], {self.x: x, self.t: t})

    def save(self, path):
        if self.saver is None:
            self.saver = tf.train.Saver(self.sess)
        self.saver.save(self.sess, path)
        return

    def load(self, path):
        if self.saver is None:
            self.saver = tf.train.Saver(self.sess)
        self.saver.restore(self.sess, path)
        return