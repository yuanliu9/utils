# draw overlay on an existing image using opencv

import cv2
import numpy as np


def draw_pts(img: np.ndarray, pts: np.ndarray, data_format='NHWC') -> np.ndarray:
    """
    draw a sequence of points onto image
    :param img:  raw image
    :param pts:  A 3-D tensor, typical shape (batch_size, num_points, 2)
    :return:
    """
    assert type(img) == np.ndarray
    assert img.ndim == 3
    try:
        assert data_format == 'NHWC'
    except AssertionError:
        print('data_format', data_format, 'is not implemented')
    for i in range(pts.shape[0]):
       img[i] = draw_pts_kernel(img[i], pts[i])
    return img


def draw_pts_kernel(img: np.ndarray, pts) -> np.ndarray:
    # pts: array-like
    for i in range(pts.shape[0]):
        cv2.circle(img, tuple(pts[i].astype(int)), 1, (0, 255, 0), 1)
    return img


def draw_bbx(img: np.ndarray, bbx: np.ndarray, data_format='NHWC') -> np.ndarray:
    assert type(img) == np.ndarray
    assert img.ndim == 3
    try:
        assert data_format == 'NHWC'
    except AssertionError:
        print('data_format', data_format, 'is not implemented')
    for i in range(bbx.shape[0]):
        img[i]= draw_bbx_kernel(img[i], bbx[i])
    return img


def draw_bbx_kernel(img: np.ndarray, bbx: np.ndarray) -> np.ndarray:
    """

    :param img:
    :param bbx: A 1-D tensor with 4 elements, (x_min, y_min, x_max, y_max)
    :return:
    """
    assert bbx.dtype == int
    assert bbx.ndim == 1
    assert bbx.shape[0] == 4
    img = cv2.rectangle(img, (bbx[0], bbx[1]), (bbx[2], bbx[3]), (0, 255, 0), 2)
    return img
