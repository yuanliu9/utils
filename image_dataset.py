import os
import numpy as np
import tensorflow as tf
from scipy.misc import imsave
from tqdm import tqdm

def write_image_dataset(x: np.ndarray, y: np.ndarray, dstpath: str):
    """
    
    :param x: 
    :param y:
    :return: 
    """
    assert x.ndim == 3 or x.ndim == 4
    assert y.ndim == 1
    if x.dtype == np.float64:
        x = (x * 256).astype(np.uint8)
    if x.dtype == np.float32:
        x = (x * 256).astype(np.uint8)
        #
    assert x.dtype == np.uint8
    for i in range(x.shape[0]):
        path = os.path.join(dstpath, str(y[i]))
        if not os.path.isdir(path):
            os.mkdir(path)
        imsave(os.path.join(path, str(i)+'.png'), x[i])
    return


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def write_tf_record(x: np.ndarray, y_:np.ndarray, dstpath: str):
    n = x.shape[0]
    assert n == y_.shape[0]
    h = x.shape[1]
    w = x.shape[2]

    print('Writing tfrecords to ', dstpath)
    with tf.python_io.TFRecordWriter(dstpath) as writer:
        for index in tqdm(range(n)):
            image_raw = x[index].tostring()
            example = tf.train.Example(
                features = tf.train.Features(
                    feature={
                        'height': _int64_feature(h),
                        'width': _int64_feature(w),
                        'label': _int64_feature(int(y_[index])),
                        'image_raw': _bytes_feature(image_raw)
                    }
                )
            )
        writer.write(example.SerializeToString())

