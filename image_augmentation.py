# this script operate image using tensorflow as backend

# July 2018

# Assume image are either in NHWC or HWC format


import tensorflow as tf

def random_map(fn):
    return tf.cond(
            pred= tf.random.uniform() > 0.5,
            true_fn = fn,
            false_fn = tf.identity)

def random_flip_left_right(x):
    return random_map(tf.image.flip_left_right)(x)


def random_translate(x: tf.Tensor, max_shift_x=0.3, max_shift_y=0.3):
    if x.shape.ndims == 4:
        # NHWC format
        raise NotImplementedError
    elif x.shape.ndims == 3:
        # HWC format
        raise NotImplementedError
    else:
        raise ValueError('Unsupported image format')
    img = tf.contrib.image.translate(x, tf.random_uniform(shape=(2,), minval=-30, maxval=30))
    return


